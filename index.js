'use strict';

const Hapi = require('@hapi/hapi');
const API = require('./api');

const init = async () => {

    const server = Hapi.server({
        port: 3001,
        host: 'localhost'
    });

    await server.register(API);

    await server.start();
    console.log('Stock running on %s', server.info.uri);
};

init();
