
'use strict';


// Hapi framework
const Hapi = require('@hapi/hapi');
// our plugin in the file api.js
const ApiPlugin = require('../api');

// assertion library
const Code = require('@hapi/code');
// Lab test runner
const Lab = require('@hapi/lab');

// instance of our test suite
const lab = exports.lab = Lab.script();

// let's get a server instance
const getServer = function () {
    // We create a server object
    const server = new Hapi.Server();

    server.start();

    // register the plugin
    return server.register(ApiPlugin)
        .then(() => server);
};

lab.test('Ensure that the server exists', () => {

    getServer()
        .then((server) => {

            Code.expect(server).to.exist();
        })
        .catch((err) => {

            console.log('error on test', err);
        });
});

lab.test('Simple GET the list of items in the database.', () => {

    // What we will inject into the server
    const toInject = {
        method: 'GET',
        url: '/stock'
    };

    getServer()
        // Inject let us pass a request to the server event if it is not started
        .then((server) => server.inject(toInject))
        // The server's response is given in a promise (or a callback if we wanted)
        .then((response) => {

            // we did not specified any other status-code, so 200 is th default one
            Code.expect(response.statusCode).to.equal(200);
            // We parse the payload as a JSON object
            const payload = JSON.parse(response.payload);
            // payload exists
            Code.expect(payload).to.exist();
            // it has the fields named: 'alive', 'item' and 'parameter'
            // Code.expect(payload).to.contain(['alive', 'item', 'parameter']);

            // The values of each field is the one we expect
            // Code.expect(payload.alive).to.be.true();
            // Code.expect(payload.item).to.equal(10);
            // Code.expect(payload.parameter).to.equal('hello');
        })
        .catch((err) => {

            console.log('error on test', err);
        });
});

lab.test('Simple example of a bad request', () => {

    // What we will inject into the server
    const toInject = {
        method: 'GET',
        url: '/stock/-1'
    };

    getServer()
    // Inject let us pass a request to the server event if it is not started
        .then((server) => server.inject(toInject))
        // The server's response is given in a promise (or a callback if we wanted)
        .then((response) => {

            // we issued a bad request, the proper response status-code is 400 then
            Code.expect(response.statusCode).to.equal(400);
            /**
             * response.payload is
             * {
             *   "statusCode":400,
             *   "error":"Bad Request","message":"child \"item\" fails because [\"item\" must be less than or equal to 100]",
             *   "validation":{"source":"query","keys":["item"]}
             * }
             */
        })
        .catch((err) => {

            console.log('error on test', err);
        });
});
