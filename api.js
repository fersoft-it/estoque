
'use strict';

const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const itemSchema = Joi.object({
    id: Joi.number().required(),
    provider: Joi.number().required(),
    name: Joi.string().required(),
    description: Joi.string(),
    price: Joi.number().required(),
    qty: Joi.number().required(),
    createdAt: Joi.date().iso(),
    modifiedAt: Joi.date().iso()
});

const db = [
    {
        id: 1,
        provider: 1,
        name: 'Console PS4 500GB',
        description: 'Playstation 4, 500 GB, 2 controles',
        price: 2799.00,
        qty: 10,
        createdAt: '2019-08-18T21:37:54.815Z'
    },
    {
        id: 2,
        provider: 2,
        name: 'Controle PS4',
        description: 'Controle Playstation 4',
        price: 279.90,
        qty: 20,
        createdAt: '2019-08-18T21:37:54.815Z'
    }
];
let idItem = 0;

const plugin = {
    name: 'estoqueAPI',
    version: '1.0.0',
    register: (server, options) => {

        server.route({
            method: 'POST',
            path: '/stock',
            handler: (request, h) => {

                idItem = idItem + 1;
                const item = Object.assign({ ...request.payload }, { createdAt: new Date(), id: idItem });

                db.push(item);
                return item;
            },
            options: {
                validate: {
                    payload: {
                        provider: Joi.number().required(),
                        name: Joi.string().min(3).required(),
                        description: Joi.string().min(0),
                        price: Joi.number().required(),
                        qty: Joi.number().required()
                    }
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/stock',
            handler: (request, h) => {

                return db;
            },
            options: {
                response: {
                    schema: Joi.array().items(itemSchema),
                    failAction: 'log'
                }
            }
        });

        server.route({
            method: 'GET',
            path: '/stock/{id}',
            handler: (request, h) => {

                const filter = db.filter((item) => item.id === +request.params.id);
                const result = filter ? filter[0] : {};
                if (!result) {
                    return h.status(404);
                }

                return result;
            }
        });

        server.route({
            method: 'GET',
            path: '/stock/search/{q}',
            handler: (request, h) => {

                const result = db.filter((item) => {

                    item.name.toLowerCase().includes(request.params.q.toLowerCase())
                    || item.description.toLowerCase().includes(request.params.q.toLowerCase());
                });

                if (!result) {

                    return h.status(404);
                }

                return result;
            }
        });

        server.route({
            method: 'PUT',
            path: '/stock',
            handler: (request, h) => {

                const payload = request.payload;
                const item = db
                    .filter((f) => f.id === payload.id)
                    .map((m) => {

                        m.provider = payload.provider;
                        m.name = payload.name;
                        m.description = payload.description;
                        m.price = payload.price;
                        m.qty = payload.qty;
                        m.modifiedAt = new Date();
                        return m;
                    })[0];

                return item;
            },
            options: {
                validate: {
                    payload: {
                        id: Joi.number().required(),
                        provider: Joi.number().required(),
                        name: Joi.string().min(3).required(),
                        description: Joi.string().min(0),
                        price: Joi.number().required(),
                        qty: Joi.number().required()
                    }
                }
            }
        });

        server.route({
            method: 'DELETE',
            path: '/stock',
            handler: (request, h) => {

                let item;
                let i;
                for (i = 0; i <= db.length; ++i) {
                    if (db[i].id === request.payload.id) {
                        item = Object.assign(db[i],item);
                        db.splice(i,1);
                    }
                }

                if (!item) {
                    return Boom.notFound('Item not found');
                }

                return item;
            },
            options: {
                validate: {
                    payload: {
                        id: Joi.number().required()
                    }
                }
            }
        });
    }
};

module.exports = plugin;
